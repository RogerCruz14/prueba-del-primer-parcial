//Desarrollar una función en Javascript que permita 
//recorrer los números del 1 al 100 y contar cuantos 
//múltiplos de 7 existen entre el 1 y el 100. Poner 
//el código en la respuesta aquí Y enviar el enlace 
//de GitLab al correo institucional patricia.quiroz@uleam.edu.ec *


iterador=1;
numero=0;
//ciclo Do-While
do {
    console.log(iterador)

    iterador=iterador+1;
    if( iterador%7==0){
        numero= numero+1;
    }

}while(iterador<101)

console.log("Cantidad de número múltiplos de 7 entre 1 al 100-->", numero);